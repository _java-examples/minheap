package org.tastefuljava.examples.minheap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MinHeapTest {
    
    public MinHeapTest() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testIt() {
        MinHeap heap = new MinHeap(10);
        List<Integer> list = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10; ++i) {
            int x = random.nextInt();
            list.add(x);
            heap.insert(x);
        }
        for (int i = 10; i < 300; ++i) {
            int x = random.nextInt();
            list.add(x);
            if (x > heap.getMin()) {
                heap.downHeap(x);
            }
        }
        Collections.sort(list);
        int n = list.size()-10;
        for (int i = 0; i < 10; ++i) {
            int x = heap.popMin();
            int xx = list.get(n+i);
            assertEquals(xx, x);
        }
    }
}
