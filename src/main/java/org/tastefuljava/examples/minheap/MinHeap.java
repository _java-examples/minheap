package org.tastefuljava.examples.minheap;

class MinHeap {
    private final int[] array;
    private int size;

    public MinHeap(int capacity) {
        array = new int[capacity];
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isFull() {
        return size == array.length;
    }

    public void insert(int value) {
        if (size == array.length) {             
            throw new ArrayIndexOutOfBoundsException("Heap is full: " + size);
        }

        int i = size++;
        int p;
        int v;
        while (i > 0 && value < (v = array[p = parent(i)])) {
            array[i] = v;
            i = p;
        }
        array[i] = value;
    }

    public int getMin() {
        if (size == 0) {             
            throw new ArrayIndexOutOfBoundsException("Heap is empty");
        }
        return array[0];
    }

    public int popMin() {
        if (size == 0) {             
            throw new ArrayIndexOutOfBoundsException("Heap is empty");
        }
 
        int root = array[0];
        if (--size > 0) {
            downHeap(array[size]);
        }
        return root;
    }

    public void downHeap(int value) {
        int i = 0;
        while (true) {
            int l = left(i);
            int r = l+1;

            int smallest = i;
            int nextVal = value;
            int v;
            if (l < size && (v = array[l]) < nextVal) {
                smallest = l;
                nextVal = v;
            }
            if (r < size && (v = array[r]) < nextVal) {
                smallest = r;
                nextVal = v;
            }
            if (smallest == i) {
                break;
            }
            array[i] = nextVal;
            i = smallest;
        }
        array[i] = value;
    }

    private static int parent(int key) {
        return (key - 1) / 2;
    }

    private static int left(int key) {
        return 2 * key + 1;
    }
}
